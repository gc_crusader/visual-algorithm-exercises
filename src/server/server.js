const express =  require("express");
const livereload = require("livereload");
const connectLivereload = require("connect-livereload");

const liveReloadServer = livereload.createServer();
liveReloadServer.server.once("connection", () => {
  setTimeout(() => {
    liveReloadServer.refresh("/");
  }, 1);
});


const server = express();
server.use(connectLivereload());
server.use(express.static("dist"));

server.listen(4242, () => console.log("Server is running... on port 4242"));